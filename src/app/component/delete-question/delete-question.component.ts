import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ApiQuestionService} from '../../service/apiQuestion.service';

@Component({
  selector: 'app-delete-question',
  templateUrl: './delete-question.component.html',
  styleUrls: ['./delete-question.component.css']
})
export class DeleteQuestionComponent implements OnInit {

  id = null;
  @Input() existingListQuestion;
  showMsgQuestionDeleted = false;
  msgQuestionDeleted = '';
  @Output() showListQuestionsCompleteFromDeleteButton = new EventEmitter<number>();

  constructor(
    private apiQuestionService: ApiQuestionService
  ) {
  }

  deleteQuestion(id: number) {
    this.apiQuestionService.deleteQuestionById(id).subscribe();
    this.showMsgQuestionDeleted = true;
    this.msgQuestionDeleted = 'Question with id ' + id + ' has been successfully deleted';
    this.showListQuestionsCompleteFromDeleteButton.emit(id);
  }

  ngOnInit() {
  }

}
