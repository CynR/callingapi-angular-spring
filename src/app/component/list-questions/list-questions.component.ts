import {Component, OnInit, OnChanges, Output, Input} from '@angular/core';
import {Question} from '../../model/question';
import {ApiQuestionService} from '../../service/apiQuestion.service';
import {EnumTypeQuestion} from '../../model/enumTypeQuestion';
import {NewQuestionMultiple} from '../../model/newQuestionMultiple';
import {NewQuestionOpen} from '../../model/newQuestionOpen';
import {NewQuestionTrueFalse} from '../../model/newQuestionTrueFalse';

@Component({
  selector: 'app-list-questions',
  templateUrl: './list-questions.component.html',
  styleUrls: ['./list-questions.component.css']
})
export class ListQuestionsComponent implements OnInit, OnChanges {

  @Output() questionList: Question[];
  newQuestionOpen: NewQuestionOpen;
  newQuestionTrueFalse: NewQuestionTrueFalse;
  type: string;
  enonce = '';
  suggestions = '';
  reponse = null;
  showMsgQuestionInserted = false;
  textQuestionInserted = 'Question inserted';
  showMsgErrorMissingInfo = false;
  textErrorMissingInfo = 'PLEASE insert information in ALL the inputs';
  // variables to manage textBoxes to enter the correct data according the type of question selected
  showMultipleChoiceTextBoxes = false;
  showTrueFalseTextBox = false;
  showOpenTextBox = false;
  filterList: Question[] = [];
  // CREAT COMBO BOX TO MAKE THE USER CHOSE BETWEEN TRUE OR FALSE !!
  public optionsTrueFalse: Array<{ title: string, value: boolean }> = [
    {title: 'True', value: true},
    {title: 'False', value: false}
  ];
  @Input() showListQuestionsComplete = false;
  showSortedQuestions = false;

  constructor(
    private apiQuestionService: ApiQuestionService
  ) {
  }

  ngOnInit() {
    this.apiQuestionService.getAllQuestions().subscribe(reponse => {
      this.questionList = reponse;
    });
    this.showListQuestionsComplete = true;
  }

  ngOnChanges() {
  }

  selectTypeQuestion(enumTypeQuestion: EnumTypeQuestion) {
    this.clearValues();
    switch (enumTypeQuestion) {
      case EnumTypeQuestion.TrueFalseToChose:
        this.type = 'TRUE_FALSE';
        this.showTrueFalseTextBox = true;
        this.showMultipleChoiceTextBoxes = false;
        this.showOpenTextBox = false;
        this.enonce = '';
        break;
      case EnumTypeQuestion.MultipleChoiceToChose:
        this.type = 'MULTIPLE';
        this.showMultipleChoiceTextBoxes = true;
        this.showOpenTextBox = true;
        this.showTrueFalseTextBox = false;
        this.enonce = '';
        this.reponse = '';
        break;
      case EnumTypeQuestion.OpenQuestionToChose:
        this.type = 'OPEN';
        this.showOpenTextBox = true;
        this.showMultipleChoiceTextBoxes = false;
        this.showTrueFalseTextBox = false;
        this.reponse = '';
        this.enonce = '';
        break;
      default:
        this.type = 'OPEN';
        this.showOpenTextBox = true;
        this.showMultipleChoiceTextBoxes = false;
        this.showTrueFalseTextBox = false;
        this.reponse = '';
        this.enonce = '';
        break;
    }
    return this.type;
  }

  select(enumTypeQuestion: EnumTypeQuestion) {
    this.clearValues();
    this.showSortedQuestions = true;
    this.showListQuestionsComplete = false;
    switch (enumTypeQuestion) {
      case EnumTypeQuestion.TrueFalseToChose:
        this.type = 'TRUE_FALSE';
        this.apiQuestionService.getAllQuestions().subscribe(reponse => {
          this.questionList = reponse;
        });
        this.filterList = this.questionList.filter(question => question.type === 'TRUE_FALSE');
        return this.filterList;
      case EnumTypeQuestion.MultipleChoiceToChose:
        this.type = 'MULTIPLE';
        this.apiQuestionService.getAllQuestions().subscribe(reponse => {
          this.questionList = reponse;
        });
        this.filterList = this.questionList.filter(question => question.type === 'MULTIPLE');
        return this.filterList;
      case EnumTypeQuestion.OpenQuestionToChose:
        this.type = 'OPEN';
        this.apiQuestionService.getAllQuestions().subscribe(reponse => {
          this.questionList = reponse;
        });
        this.filterList = this.questionList.filter(question => question.type === 'OPEN');
        return this.filterList;
      default:
        this.apiQuestionService.getAllQuestions().subscribe(reponse => {
          this.questionList = reponse;
        });
        this.filterList = this.questionList.filter(question => question.type = 'OPEN');
        return this.filterList;
    }
  }

  showAllQuestions() {
    this.showListQuestionsComplete = true;
    this.showSortedQuestions = false;
    this.apiQuestionService.getAllQuestions().subscribe(reponse => {
      this.questionList = reponse;
    });
  }

  validation() {
    switch (this.type) {
      case 'TRUE_FALSE':
        if (this.enonce.length === 0 || this.reponse.length === 0) {
          this.showMsgErrorMissingInfo = true;
          this.showMsgQuestionInserted = false;
        } else {
          this.showMsgQuestionInserted = true;
          this.showMsgErrorMissingInfo = false;
          // be sure of receiving boolean JSON.parse
          this.newQuestionTrueFalse = new NewQuestionTrueFalse(this.type, this.enonce, JSON.parse(this.reponse));
          this.apiQuestionService.creatQuestionTrueFalse(this.newQuestionTrueFalse).subscribe(answer => {
            this.newQuestionTrueFalse = answer;
          })
          ;
          this.showListQuestionsComplete = true;
        }
        break;
      case 'MULTIPLE':
        if (this.enonce.length === 0 || this.suggestions.length === 0 || this.reponse.length === 0) {
          this.showMsgErrorMissingInfo = true;
          this.showMsgQuestionInserted = false;
        } else {
          this.showMsgQuestionInserted = true;
          this.showMsgErrorMissingInfo = false;
          const listSuggestions = [];
          for (const suggestion of this.suggestions.split(',')) {
            listSuggestions.push(suggestion.trim());
          }
          // be sure to send the good type of valor in this.suggestions
          const newQuestionMultiple = new NewQuestionMultiple(this.type, this.enonce, listSuggestions, this.reponse);
          this.apiQuestionService.creatQuestionMultiple(newQuestionMultiple).subscribe();
          this.showListQuestionsComplete = true;
        }
        break;
      case 'OPEN':
        if (this.enonce.length === 0 || this.reponse.length === 0) {
          this.showMsgErrorMissingInfo = true;
          this.showMsgQuestionInserted = false;
        } else {
          this.showMsgQuestionInserted = true;
          this.showMsgErrorMissingInfo = false;
          this.newQuestionOpen = new NewQuestionOpen(this.type, this.enonce, this.reponse);
          this.apiQuestionService.createQuestionOpen(this.newQuestionOpen).subscribe(answer => {
            this.newQuestionOpen = answer;
          });
        }
        this.showListQuestionsComplete = true;
    }
    this.apiQuestionService.getAllQuestions().subscribe(reponse => {
      this.questionList = reponse;
    });
    setTimeout(() => {
      this.showMsgQuestionInserted = false;
      this.showMsgErrorMissingInfo = false;
    }, 5000);
  }

  showRemainingQuestions(id: number) {
    this.showListQuestionsComplete = false;
    this.questionList = this.questionList.filter(q => q.id !== id);
    this.showListQuestionsComplete = true;
  }

  clearValues() {
    this.enonce = '';
    this.suggestions = '';
    this.reponse = '';
    this.showMsgQuestionInserted = false;
    this.showMsgErrorMissingInfo = false;
  }

}

