import {Component, Input, OnInit} from '@angular/core';
import {Question} from '../../model/question';
import {ApiQuestionService} from '../../service/apiQuestion.service';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

  @Input() question: Question;

  constructor(
    private apiQuestionService: ApiQuestionService
  ) {
  }

  ngOnInit() {
    this.apiQuestionService.getQuestion().subscribe(reponse => {
      console.log(reponse);
      }, error => {
      console.log(error);
    });
  }

}
