import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TryingAnswerComponent } from './trying-answer.component';

describe('TryingAnswerComponent', () => {
  let component: TryingAnswerComponent;
  let fixture: ComponentFixture<TryingAnswerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TryingAnswerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TryingAnswerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
