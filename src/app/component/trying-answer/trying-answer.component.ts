import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ApiQuestionService} from '../../service/apiQuestion.service';
import {AnswerRepresentation} from '../../model/answerRepresentation';
import {AnswerReceived} from '../../model/answerReceived';

@Component({
  selector: 'app-trying-answer',
  templateUrl: './trying-answer.component.html',
  styleUrls: ['./trying-answer.component.css']
})
export class TryingAnswerComponent implements OnInit {

  id = null;
  @Input() existingListQuestionToAnswer;
  showMsgQuestionCorrect = false;
  showMsgQuestionIncorrect = false;
  MsgQuestionCorrect = '';
  MsgQuestionIncorrect = '';
  @Output() showListQuestionsCompleteFromTryAnswer = new EventEmitter<boolean>();
  answer = '';
  answerRepresentation: AnswerRepresentation;
  answerReceived: AnswerReceived;

  constructor(
    private apiQuestionService: ApiQuestionService
  ) {
  }

  answerQuestion(id: number) {
    this.answerRepresentation = new AnswerRepresentation(this.answer);
    this.apiQuestionService.tryAnswer(id, this.answerRepresentation).subscribe(answer => {
      this.answerReceived = answer;
      console.log(this.answerReceived.result);
      if (this.answerReceived.result === 'CORRECT') {
        this.showMsgQuestionCorrect = true;
        this.MsgQuestionCorrect = 'Well done, is the Correct Answer';
        this.showMsgQuestionIncorrect = false;
      } else if (this.answerReceived.result === 'ALMOST_CORRECT') {
        this.showMsgQuestionCorrect = true;
        this.MsgQuestionCorrect = 'Almost correct';
        this.showMsgQuestionIncorrect = false;
      } else {
      this.showMsgQuestionIncorrect = true;
      this.MsgQuestionIncorrect = 'Bad answer, try harder';
      this.showMsgQuestionCorrect = false;
      }
    });
  }

  ngOnInit() {
  }

}
