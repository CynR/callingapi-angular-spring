import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Question} from '../model/question';
import {NewQuestionMultiple} from '../model/newQuestionMultiple';
import {NewQuestionTrueFalse} from '../model/newQuestionTrueFalse';
import {NewQuestionOpen} from '../model/newQuestionOpen';
import {AnswerRepresentation} from '../model/answerRepresentation';
import {AnswerReceived} from '../model/answerReceived';

@Injectable()
export class ApiQuestionService {

  constructor(
    private http: HttpClient
  ) {
  }

  getQuestion(): Observable<Question> {
    return this.http.get<Question>('http://localhost:8080/questions/1');
  }

  getAllQuestions(): Observable<Question[]> {
    return this.http.get<Question[]>('http://localhost:8080/questions');
  }

  creatQuestionMultiple(question: NewQuestionMultiple): Observable<NewQuestionMultiple> {
    return this.http.post<NewQuestionMultiple>('http://localhost:8080/questions', question);
  }

  creatQuestionTrueFalse(question: NewQuestionTrueFalse): Observable<NewQuestionTrueFalse> {
    return this.http.post<NewQuestionTrueFalse>('http://localhost:8080/questions', question);
  }

  createQuestionOpen(question: NewQuestionOpen): Observable<NewQuestionOpen> {
    return this.http.post<NewQuestionOpen>('http://localhost:8080/questions', question);
  }

  deleteQuestionById(id: number): Observable<any> {
    return this.http.delete<any>('http://localhost:8080/questions/' + id);
  }

  tryAnswer(id: number, reponse: AnswerRepresentation): Observable<AnswerReceived> {
    return this.http.post<AnswerReceived>('http://localhost:8080/questions/' + id + '/answer', reponse);
  }

}

