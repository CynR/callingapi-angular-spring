export class OptionsTrueFalse {
  public options: Array<{option: boolean}> = [
    {option: true},
    {option: false}
  ];
}
