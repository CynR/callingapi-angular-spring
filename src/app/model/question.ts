export class Question {
  id: number;
  type: string;
  enonce: string;
  suggestions: string[];
}
