import {Injectable} from '@angular/core';
import {Question} from './question';

@Injectable()
export class ListQuestionsModel {
  listAllQuestions: Question[];
}
