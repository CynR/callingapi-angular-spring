export class AnswerRepresentation {
  answer: string;

  constructor(answer: string) {
    this.answer = answer;
  }
}
