export class AnswerReceived {
  result: string;

  constructor(result: string) {
    this.result = result;
  }
}
