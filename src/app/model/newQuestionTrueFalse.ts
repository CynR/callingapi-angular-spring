export class NewQuestionTrueFalse {
  type: string;
  enonce: string;
  reponse: boolean;

  constructor(type: string, enonce: string, reponse: boolean) {
    this.type = type;
    this.enonce = enonce;
    this.reponse = reponse;
  }
}
