import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { QuestionComponent } from './component/question/question.component';
import { ApiQuestionService } from './service/apiQuestion.service';
import { HttpClientModule } from '@angular/common/http';
import { ListQuestionsComponent } from './component/list-questions/list-questions.component';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DeleteQuestionComponent } from './component/delete-question/delete-question.component';
import { TryingAnswerComponent } from './component/trying-answer/trying-answer.component';

@NgModule({
  declarations: [
    AppComponent,
    QuestionComponent,
    ListQuestionsComponent,
    DeleteQuestionComponent,
    TryingAnswerComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
  ],
  providers: [
    ApiQuestionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
